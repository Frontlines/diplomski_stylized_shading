using System.Collections.Generic;
using UnityEngine;

public struct MatCapData
{
    public Texture ForwardTexture;
    public int ForwardTextureIndex;
    public Texture BackTexture;
    public int BackTextureIndex;
    public Texture ReflectionTexture;
    public int ReflectionTextureIndex;
    public float MatCapRotation;
    public float ReflectionStrength;
    public float InterpolationRange;
    public bool InverseSpecular;
    public float SpecularIntensity;
    public float SpecularRange;
    public float SpecularFuzziness;
    public bool UseAlphaColorClipping;
    public Color AlphaColor;
    public float AlphaRange;
    public float AlphaFuzziness;
    public bool UseLighting;

    public static MatCapData Default
    {
        get
        {
            var data = new MatCapData();

            data.ForwardTextureIndex = 0;
            data.BackTextureIndex = 0;
            data.ReflectionTextureIndex = 0;
            data.MatCapRotation = 0f;
            data.ReflectionStrength = 1f;
            data.InterpolationRange = 0.1f;
            data.InverseSpecular = false;
            data.SpecularIntensity = 1f;
            data.SpecularRange = 0.5f;
            data.SpecularFuzziness = 0.25f;
            data.UseAlphaColorClipping = false;
            data.AlphaColor = new Color(1f, 1f, 1f, 1f);
            data.AlphaRange = 0.5f;
            data.AlphaFuzziness = 0.25f;
            data.UseLighting = false;

            return data;
        }
    }
}

[RequireComponent(typeof(Renderer))]
public class MatCapObject : MonoBehaviour
{
    #region Constants

    private const string ForwardTextureID = "_MatCapForwardMap";
    private const string BackTextureID = "_MatCapBackMap";
    private const string ReflectionTextureID = "_MatCapReflectionMap";
    private const string MatCapRotationID = "_MatCapRotation";
    private const string ReflectionStrengthID = "_ReflectionStrength";
    private const string InterpolationRangeID = "_InterpolationRange";
    private const string InverseSpecularID = "_InverseSpecular";
    private const string SpecularIntensityID = "_SpecularIntensity";
    private const string SpecularRangeID = "_SpecularRange";
    private const string SpecularFuzzinessID = "_SpecularFuzziness";
    private const string UseAlphaColorClippingID = "_UseAlphaColorClipping";
    private const string AlphaColorID = "_AlphaColor";
    private const string AlphaRangeID = "_AlphaRange";
    private const string AlphaFuzzinessID = "_AlphaFuzziness";
    private const string UseLightingID = "_UseLighting";
    private const string UseOutlineID = "_UseOutline";

    #endregion

    private static List<MatCapObject> _matCapObjects = new List<MatCapObject>(30);

    private Renderer _renderer;
    private MatCapData _data;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();

        InitializeData();
    }

    private void Start()
    {
        _matCapObjects.Add(this);
    }

    private void OnDestroy()
    {
        _matCapObjects.Remove(this);
    }

    private void InitializeData()
    {
        _data = MatCapData.Default;

        _data.ForwardTexture = _renderer.material.GetTexture(ForwardTextureID);
        _data.BackTexture = _renderer.material.GetTexture(BackTextureID);
        _data.ReflectionTexture = _renderer.material.GetTexture(ReflectionTextureID);
        _data.MatCapRotation = _renderer.material.GetFloat(MatCapRotationID);
        _data.ReflectionStrength = _renderer.material.GetFloat(ReflectionStrengthID);
        _data.InterpolationRange = _renderer.material.GetFloat(InterpolationRangeID);
        _data.InverseSpecular = _renderer.material.GetFloat(InverseSpecularID) > 0f;
        _data.SpecularIntensity = _renderer.material.GetFloat(SpecularIntensityID);
        _data.SpecularRange = _renderer.material.GetFloat(SpecularRangeID);
        _data.SpecularFuzziness = _renderer.material.GetFloat(SpecularFuzzinessID);
        _data.UseAlphaColorClipping = _renderer.material.GetFloat(UseAlphaColorClippingID) > 0f;
        _data.AlphaColor = _renderer.material.GetColor(AlphaColorID);
        _data.AlphaRange = _renderer.material.GetFloat(AlphaRangeID);
        _data.AlphaFuzziness = _renderer.material.GetFloat(AlphaFuzzinessID);
        _data.UseLighting = _renderer.material.GetFloat(UseLightingID) > 0f;
    }

    /// <summary>
    /// Gets the Mat Cap Data of the object.
    /// </summary>
    /// <returns>The Mat Cap Data of the object.</returns>
    public MatCapData GetData()
    {
        return _data;
    }

    /// <summary>
    /// Sets the Mat Cap material settings with the given data.
    /// </summary>
    /// <param name="data">The data containing the new Mat Cap settings.</param>
    public void SetData(MatCapData data)
    {
        _data = data;

        // set Mat Cap Material data
        _renderer.material.SetTexture(ForwardTextureID, _data.ForwardTexture);
        _renderer.material.SetTexture(BackTextureID, _data.BackTexture);
        _renderer.material.SetTexture(ReflectionTextureID, _data.ReflectionTexture);
        _renderer.material.SetFloat(MatCapRotationID, _data.MatCapRotation);
        _renderer.material.SetFloat(ReflectionStrengthID, _data.ReflectionStrength);
        _renderer.material.SetFloat(InterpolationRangeID, _data.InterpolationRange);
        _renderer.material.SetFloat(InverseSpecularID, _data.InverseSpecular ? 1f : 0f);
        _renderer.material.SetFloat(SpecularIntensityID, _data.SpecularIntensity);
        _renderer.material.SetFloat(SpecularRangeID, _data.SpecularRange);
        _renderer.material.SetFloat(SpecularFuzzinessID, _data.SpecularFuzziness);
        _renderer.material.SetFloat(UseAlphaColorClippingID, _data.UseAlphaColorClipping ? 1f : 0f);
        _renderer.material.SetColor(AlphaColorID, _data.AlphaColor);
        _renderer.material.SetFloat(AlphaRangeID, _data.AlphaRange);
        _renderer.material.SetFloat(AlphaFuzzinessID, _data.AlphaFuzziness);
        _renderer.material.SetFloat(UseLightingID, _data.UseLighting ? 1f : 0f);
    }

    /// <summary>
    /// Marks teh Mat Cap Object as being selected.
    /// </summary>
    /// <returns>The Mat Cap Data of this object.</returns>
    public MatCapData Select()
    {
        _renderer.material.SetFloat(UseOutlineID, 1f);

        return _data;
    }

    /// <summary>
    /// Deselects the Mat Cap Object.
    /// </summary>
    public void Deselect()
    {
        _renderer.material.SetFloat(UseOutlineID, 0f);
    }

    /// <summary>
    /// Sets the given Mat Cap Data to all the active Mat Cap Objects in the scene.
    /// </summary>
    /// <param name="data">The Mat Cap Data to apply to all active Mat Cap Objects.</param>
    public static void SetDataToAllObjects(MatCapData data)
    {
        for (var i = 0; i < _matCapObjects.Count; i++)
        {
            var matCap = _matCapObjects[i];
            matCap.SetData(data);
        }
    }
}
