﻿using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Windows.WebCam;

namespace Assets.Scripts.Camera
{
    public class WebCameraManager
    {
        private WebCamTexture _cameraTexture;

        /// <summary>
        /// The Web Camera Texture that contains pixel data of the current Web Camera feed.
        /// </summary>
        public WebCamTexture CameraTexture => _cameraTexture;

        /// <summary>
        /// The name of the device that is currently in use.
        /// </summary>
        public string DeviceName => _cameraTexture.deviceName;

        /// <summary>
        /// Is there a Web Camera Device currently in use.
        /// </summary>
        /// <returns></returns>
        public bool HasWebCamera()
        {
            return _cameraTexture;
        }

        /// <summary>
        /// Gets all available Web Camera devices.
        /// </summary>
        /// <returns>The array containing all Web Camera Devices.</returns>
        public static WebCamDevice[] GetWebCameras()
        {
            return WebCamTexture.devices;
        }

        /// <summary>
        /// Captures the current camera frame and stores it to a new texture object.
        /// </summary>
        /// <returns>The texture with the captured Web Camera frame.</returns>
        public Texture CaptureFrame()
        {
            // create a new 2D texture that is stored in memory 
            var frame = new Texture2D(_cameraTexture.width, _cameraTexture.height, GraphicsFormat.R8G8B8A8_SRGB,
                TextureCreationFlags.None);
            
            // set the anisotrophy level as high as possible, to make the texture as clear as possible
            frame.anisoLevel = 16;
            frame.SetPixels(_cameraTexture.GetPixels());
            frame.Apply();

            return frame;
        }

        /// <summary>
        /// Selects the given device as the desired Web Camera.
        /// </summary>
        /// <param name="device">The device to use as the active Web Camera.</param>
        /// <param name="autoPlay">Should the device start playing instantly.</param>
        /// <returns>The Web Camera texture that is constantly updated.</returns>
        public WebCamTexture SelectCamera(WebCamDevice device, bool autoPlay = true)
        {
            if (_cameraTexture != null && _cameraTexture.isPlaying)
            {
                _cameraTexture.Stop();
            }
            
            _cameraTexture = new WebCamTexture(device.name, 512, 512);
            _cameraTexture.requestedFPS = 24f;

            if (autoPlay)
                _cameraTexture.Play();

            return _cameraTexture;
        }

        /// <summary>
        /// Disables the current Web Camera device.
        /// </summary>
        public void UnlinkCamera()
        {
            if (_cameraTexture != null && _cameraTexture.isPlaying)
            {
                _cameraTexture.Stop();
            }

            _cameraTexture = null;
        }
    }
}
