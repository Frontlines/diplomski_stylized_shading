﻿void DirectSpecular_float(float3 Specular, float Smoothness, float3 Direction, float3 Color, float3 WorldNormal, float3 WorldView, out float3 Out)
{
#ifdef SHADERGRAPH_PREVIEW
	Smoothness = exp2(10 * Smoothness + 1);
	WorldNormal = normalize(WorldNormal);
	WorldView = SafeNormalize(WorldView);
	
	float3 halfVec = SafeNormalize(float3(Direction) + float3(WorldView));
	float NdotH = saturate(dot(WorldNormal, halfVec));
	float modifier = pow(NdotH, Smoothness);
	float3 specularReflection = Specular.rgb * modifier;
	Out = Color * specularReflection;
#else
	Smoothness = exp2(10 * Smoothness + 1);
	WorldNormal = normalize(WorldNormal);
	WorldView = SafeNormalize(WorldView);
	Out = LightingSpecular(Color, Direction, WorldNormal, WorldView, float4(Specular, 0), Smoothness);
#endif
}

void DirectSpecular_half(half3 Specular, half Smoothness, half3 Direction, half3 Color, half3 WorldNormal, half3 WorldView, out half3 Out)
{
#ifdef SHADERGRAPH_PREVIEW
	Smoothness = exp2(10 * Smoothness + 1);
	WorldNormal = normalize(WorldNormal);
	WorldView = SafeNormalize(WorldView);
	
	half3 halfVec = SafeNormalize(float3(Direction)+float3(WorldView));
	half NdotH = saturate(dot(WorldNormal, halfVec));
	half modifier = pow(NdotH, Smoothness);
	half3 specularReflection = Specular.rgb * modifier;
	Out = Color * specularReflection;
#else
	Smoothness = exp2(10 * Smoothness + 1);
	WorldNormal = normalize(WorldNormal);
	WorldView = SafeNormalize(WorldView);
	Out = LightingSpecular(Color, Direction, WorldNormal, WorldView, half4(Specular, 0), Smoothness);
#endif
}