﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(RawImage))]
    public class TextureSelectionUI : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private Texture[] _textures;

        private RawImage _image;

        public Texture SelectedTexture
        {
            get => _image.texture;
            set => _image.texture = value;
        }

        public int Index { get; set; } = 0;

        private void Awake()
        {
            _image = this.GetComponent<RawImage>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                NextTexture();
            }
            else if (eventData.button == PointerEventData.InputButton.Middle)
            {
                PrevTexture();
            }
            else if (eventData.button == PointerEventData.InputButton.Right)
            {
                PrevTexture();
            }
        }

        public void NextTexture()
        {
            Index++;
            
            if (Index >= _textures.Length)
            {
                Index = 0;
            }

            SelectedTexture = _textures[Index];
        }

        public void PrevTexture()
        {
            Index--;
            
            if (Index < 0)
            {
                Index = _textures.Length - 1;
            }

            SelectedTexture = _textures[Index];
        }
    }
}
