using Assets.Scripts.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MatCapSettingsUI : MonoBehaviour
{
    private enum SettingsApplicationMode
    {
        Global,
        Selection
    }

    private SettingsApplicationMode _mode = SettingsApplicationMode.Global;
    private MatCapData _globalMatCapData = MatCapData.Default; 

    private Camera _mainCamera = null;
    private RaycastHit[] _hits = new RaycastHit[10];

    private MatCapObject _selectedMatCap = null;
    private MatCapObject SelectedMatCap
    {
        get => _selectedMatCap;
        set
        {
            if (_selectedMatCap)
            {
                _selectedMatCap.Deselect();
            }

            _selectedMatCap = value;

            if (_selectedMatCap && _mode == SettingsApplicationMode.Selection)
            {
                var data = _selectedMatCap.Select();
                DisplayMatCapData(data);
            }
        }
    }

    #region ComponentReferences

    [SerializeField]
    private Toggle _globalToggle;

    [SerializeField]
    private Toggle _selectionToggle;

    public TextureSelectionUI ForwardTextureSelection;

    public TextureSelectionUI BackTextureSelection;

    public TextureSelectionUI ReflectionTextureSelection;

    [SerializeField]
    private InputField _matCapRotationInput;

    [SerializeField]
    private InputField _reflectionStrengthInput;

    [SerializeField]
    private InputField _interpolationRangeInput;

    [SerializeField]
    private Toggle _invertSpecularToggle;

    [SerializeField]
    private InputField _specularIntensityInput;

    [SerializeField]
    private InputField _specularRangeInput;

    [SerializeField]
    private InputField _specularFuzzinessInput;

    [SerializeField]
    private Toggle _useAlphaColorClipping;

    [SerializeField]
    private FlexibleColorPicker _alphaColorPicker;

    [SerializeField]
    private InputField _alphaRangeInput;

    [SerializeField]
    private InputField _alphaFuzzinessInput;

    [SerializeField]
    private Toggle _useLightingToggle;

    #endregion

    private void Awake()
    {
        _mainCamera = Camera.main;
    }

    private void Start()
    {
        // by default use global settings
        SetGlobalSettings();
    }

    private void Update()
    {
        var isPointerOverUI = EventSystem.current.IsPointerOverGameObject();

        // select an object using the left mouse button only if the pointer isn't over an UI object
        // determine what/if any Mat Cap Object was selected
        if (Input.GetMouseButtonUp(0) && !isPointerOverUI)
        {
            Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

            var hitCount = Physics.RaycastNonAlloc(ray, _hits, _mainCamera.farClipPlane);

            if (hitCount <= 0) return;

            MatCapObject matCap = null;
            float minDistance = float.MaxValue;

            for (var i = 0; i < hitCount; i++)
            {
                var hit = _hits[i];

                if (hit.distance < minDistance)
                {
                    matCap = hit.transform.GetComponent<MatCapObject>();
                    minDistance = hit.distance;
                }
            }

            SelectedMatCap = matCap;
        }

        // deselect the current selected object with the right mouse button
        if (Input.GetMouseButtonUp(1) && !isPointerOverUI)
        {
            SelectedMatCap = null;
        }

        // applies the current input values when the 'Enter' key is pressed
        if (Input.GetKeyUp(KeyCode.Return))
        {
            ApplyMatCapData();
        }
    }

    /// <summary>
    /// Sets the application mode to apply settings to all Mat Cap Objects.
    /// </summary>
    public void SetGlobalSettings()
    {
        if (!_globalToggle.isOn) return;

        _mode = SettingsApplicationMode.Global;

        DisplayMatCapData(_globalMatCapData);
    }

    /// <summary>
    /// Sets the application mode to apply settings only to the selected Mat Cap Object.
    /// </summary>
    public void SetSelectionSettings()
    {
        if (!_selectionToggle.isOn) return;

        _mode = SettingsApplicationMode.Selection;

        if (SelectedMatCap)
        {
            DisplayMatCapData(SelectedMatCap.GetData());
        }
    }

    /// <summary>
    /// Updates input component values with the passed Mat Cap Data values.
    /// </summary>
    /// <param name="data"></param>
    private void DisplayMatCapData(MatCapData data)
    {
        ForwardTextureSelection.SelectedTexture = data.ForwardTexture;
        ForwardTextureSelection.Index = data.ForwardTextureIndex;
        BackTextureSelection.SelectedTexture = data.BackTexture;
        BackTextureSelection.Index = data.BackTextureIndex;
        ReflectionTextureSelection.SelectedTexture = data.ReflectionTexture;
        ReflectionTextureSelection.Index = data.ReflectionTextureIndex;
        _matCapRotationInput.SetTextWithoutNotify(data.MatCapRotation.ToString());
        _reflectionStrengthInput.SetTextWithoutNotify(data.ReflectionStrength.ToString());
        _interpolationRangeInput.SetTextWithoutNotify(data.InterpolationRange.ToString());
        _invertSpecularToggle.SetIsOnWithoutNotify(data.InverseSpecular);
        _specularIntensityInput.SetTextWithoutNotify(data.SpecularIntensity.ToString());
        _specularRangeInput.SetTextWithoutNotify(data.SpecularRange.ToString());
        _specularFuzzinessInput.SetTextWithoutNotify(data.SpecularFuzziness.ToString());
        _useAlphaColorClipping.SetIsOnWithoutNotify(data.UseAlphaColorClipping);
        _alphaColorPicker.SetColor(data.AlphaColor);
        _alphaRangeInput.SetTextWithoutNotify(data.AlphaRange.ToString());
        _alphaFuzzinessInput.SetTextWithoutNotify(data.AlphaFuzziness.ToString());
        _useLightingToggle.SetIsOnWithoutNotify(data.UseLighting);
    }

    /// <summary>
    /// Reads all the values from the inputs and constructs a new MatCapData structure
    /// which is then passed to the selected Mat Cap Object or passed to all Mat Cap Objects.
    /// </summary>
    public void ApplyMatCapData()
    {
        var data = ConstructMatCapDataFromInputs();

        switch (_mode)
        {
            case SettingsApplicationMode.Global:
                _globalMatCapData = data;
                MatCapObject.SetDataToAllObjects(_globalMatCapData);
                break;
            case SettingsApplicationMode.Selection:
                SelectedMatCap?.SetData(data);
                break;
        }
    }

    /// <summary>
    /// Constructs a new Mat Cap Data structure from the current input values.
    /// </summary>
    /// <returns>The Mat Cap Data structure containing all input values.</returns>
    private MatCapData ConstructMatCapDataFromInputs()
    {
        var data = new MatCapData();

        data.ForwardTexture = ForwardTextureSelection.SelectedTexture;
        data.ForwardTextureIndex = ForwardTextureSelection.Index;
        data.BackTexture = BackTextureSelection.SelectedTexture;
        data.BackTextureIndex = BackTextureSelection.Index;
        data.ReflectionTexture = ReflectionTextureSelection.SelectedTexture;
        data.ReflectionTextureIndex = ReflectionTextureSelection.Index;
        data.MatCapRotation = float.Parse(_matCapRotationInput.text);
        data.ReflectionStrength = float.Parse(_reflectionStrengthInput.text);
        data.InterpolationRange = float.Parse(_interpolationRangeInput.text);
        data.InverseSpecular = _invertSpecularToggle.isOn;
        data.SpecularIntensity = float.Parse(_specularIntensityInput.text);
        data.SpecularRange = float.Parse(_specularRangeInput.text);
        data.SpecularFuzziness = float.Parse(_specularFuzzinessInput.text);
        data.UseAlphaColorClipping = _useAlphaColorClipping.isOn;
        data.AlphaColor = _alphaColorPicker.color;
        data.AlphaRange = float.Parse(_alphaRangeInput.text);
        data.AlphaFuzziness = float.Parse(_alphaFuzzinessInput.text);
        data.UseLighting = _useLightingToggle.isOn;

        return data;
    }
}
