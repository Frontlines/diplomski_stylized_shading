﻿using System.Linq;
using Assets.Scripts.Camera;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class WebCamUI : MonoBehaviour
    {
        private WebCameraManager _webCam;
        private WebCamDevice[] _devices;

        #region Component References

        [SerializeField]
        private Dropdown _webCameraDropdown = null;

        [SerializeField]
        private RawImage _webCameraImage = null;

        [SerializeField] 
        private MatCapSettingsUI _matCapSettings;

        #endregion

        private void Awake()
        {
            // the dropdown containing all web camera devices
            if (!_webCameraDropdown)
            {
                _webCameraDropdown = GetComponentInChildren<Dropdown>();
            }

            // the raw image component displaying the web camera feed
            if (!_webCameraImage)
            {
                _webCameraImage = GetComponentInChildren<RawImage>();
            }

            _webCam = new WebCameraManager();
        }

        private void Start()
        {
            UpdateWebCameraDevices();
        }

        /// <summary>
        /// Updates the dropdown with all available Web Camera Devices found attached to the system.
        /// </summary>
        public void UpdateWebCameraDevices()
        {
            _devices = WebCameraManager.GetWebCameras();

            if (_devices.Length <= 0)
            {
                _webCam.UnlinkCamera();
                return;
            }

            var deviceNames = _devices.Select((ld) => ld.name).ToList();
            deviceNames.Insert(0, "None");

            // if the currently is no camera active,
            // then set the first camera device as the selected device
            // and update the dropdown with the devices
            if (!_webCam.HasWebCamera())
            {
                _webCameraDropdown.ClearOptions();
                _webCameraDropdown.AddOptions(deviceNames);
                _webCameraDropdown.SetValueWithoutNotify(0);
                return;
            }

            var webCamPresent = false;

            // determine if our current camera device is still plugged in
            for (var i = 0; i < _devices.Length; i++)
            {
                var device = _devices[i];

                if (device.name.Equals(_webCam.DeviceName))
                {
                    webCamPresent = true;
                    break;
                }
            }

            // clear the device dropdown options
            _webCameraDropdown.ClearOptions();

            // add the current devices to the dropdown options
            _webCameraDropdown.AddOptions(deviceNames);

            // select the first camera if the current camera was not found
            if (!webCamPresent)
            {
                _webCameraDropdown.SetValueWithoutNotify(0);
                SelectWebCamera(0);
            }
            else
            {
                var index = _webCameraDropdown.options.FindIndex((li) => li.text.Equals(_webCam.DeviceName));

                _webCameraDropdown.SetValueWithoutNotify(index);
            }
        }

        /// <summary>
        /// Select the given camera at id.
        /// </summary>
        /// <param name="id">The id of the Web Camera Device to use.</param>
        public void SelectWebCamera(int id)
        {
            if (id <= 0 || id > _devices.Length)
            {
                _webCam.UnlinkCamera();
                return;
            }

            var selectedDevice = _devices[id - 1];

            _webCameraImage.texture = _webCam.SelectCamera(selectedDevice);
        }

        public void SetAsForwardTexture()
        {
            if (!_webCam.HasWebCamera()) return;

            var frame = _webCam.CaptureFrame();

            _matCapSettings.ForwardTextureSelection.SelectedTexture = frame;
            _matCapSettings.ForwardTextureSelection.Index = -1;
        }

        public void SetAsBackTexture()
        {
            if (!_webCam.HasWebCamera()) return;

            var frame = _webCam.CaptureFrame();

            _matCapSettings.BackTextureSelection.SelectedTexture = frame;
            _matCapSettings.BackTextureSelection.Index = -1;
        }

        public void SetAsReflectionTexture()
        {
            if (!_webCam.HasWebCamera()) return;

            var frame = _webCam.CaptureFrame();

            _matCapSettings.ReflectionTextureSelection.SelectedTexture = frame;
            _matCapSettings.ReflectionTextureSelection.Index = -1;
        }

    }
}
